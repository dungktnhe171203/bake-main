/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package bakery.controllers;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ngdugn
 */
public class AddToCartControllerTest {
    
    public AddToCartControllerTest() {
    }

    /**
     * Test of processRequest method, of class AddToCartController.
     */
    @Test
    public void testProcessRequest() throws Exception {
        System.out.println("processRequest");
        HttpServletRequest request = null;
        HttpServletResponse response = null;
        AddToCartController instance = new AddToCartController();
        instance.processRequest(request, response);
        fail("The test case is a prototype.");
    }

    /**
     * Test of doGet method, of class AddToCartController.
     */
    @Test
    public void testDoGet() throws Exception {
        System.out.println("doGet");
        HttpServletRequest request = null;
        HttpServletResponse response = null;
        AddToCartController instance = new AddToCartController();
        instance.doGet(request, response);
        fail("The test case is a prototype.");
    }

    /**
     * Test of doPost method, of class AddToCartController.
     */
    @Test
    public void testDoPost() throws Exception {
        System.out.println("doPost");
        HttpServletRequest request = null;
        HttpServletResponse response = null;
        AddToCartController instance = new AddToCartController();
        instance.doPost(request, response);
        fail("The test case is a prototype.");
    }

    /**
     * Test of getServletInfo method, of class AddToCartController.
     */
    @Test
    public void testGetServletInfo() {
        System.out.println("getServletInfo");
        AddToCartController instance = new AddToCartController();
        String expResult = "";
        String result = instance.getServletInfo();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
    
}
